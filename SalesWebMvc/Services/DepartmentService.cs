﻿using Microsoft.EntityFrameworkCore;
using SalesWebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesWebMvc.Services
{
    public class DepartmentService
    {
        private readonly SalesWebMvcContext _context;

        public DepartmentService(SalesWebMvcContext context)
        {
            _context = context;
        }

        public async Task<List<Department>> FindAllAsync() //a função está assíncrona (async) retornando um task de List Department
        {
            return await _context.Department.OrderBy(x => x.Name).ToListAsync(); //aqui tem outra chamada assíncrona, e o compilador é
            //avisao con await
        }
    }
}
