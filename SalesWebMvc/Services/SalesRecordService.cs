﻿using Microsoft.EntityFrameworkCore;
using SalesWebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesWebMvc.Services
{
    public class SalesRecordService
    {
        private readonly SalesWebMvcContext _context;
        public SalesRecordService(SalesWebMvcContext context)
        {
            _context = context;
        }

        public async Task<List<SalesRecord>> FindByDateAsync(DateTime? minDate, DateTime? maxDate)
        {
            var result = from obj in _context.SalesRecord select obj; //ainda não foi executada a query, só foi definida
            if (minDate.HasValue)
                result = result.Where(x => x.Date >= minDate.Value); // foi acrescentada 
            if (maxDate.HasValue)
                result = result.Where(x => x.Date <= maxDate.Value);
            return await result
                .Include(x => x.Seller) //faz o join da tabela Seller com a de SalesRecord
                .Include(x => x.Seller.Department)
                .OrderByDescending(x => x.Date)
                .ToListAsync();
        }

        public async Task<List<IGrouping<Department, SalesRecord>>> FindByDateGroupingAsync(DateTime? minDate, DateTime? maxDate)
        {
            var result = from obj in _context.SalesRecord select obj; //ainda não foi executada a query, só foi definida
            if (minDate.HasValue)
                result = result.Where(x => x.Date >= minDate.Value); // foi acrescentada 
            if (maxDate.HasValue)
                result = result.Where(x => x.Date <= maxDate.Value);
            return await result
                .Include(x => x.Seller) //faz o join da tabela Seller com a de SalesRecord
                .Include(x => x.Seller.Department)
                .OrderByDescending(x => x.Date)
                .GroupBy(x => x.Seller.Department)
                .ToListAsync();
        }
    }
}
